<?php
/**
 * Skin
 *
 * @category   Template engine
 * @package    skinengine/skin
 * @author     Valentin Chouaf (Laegel) <valentin.chouaf@gmail.com>
 * @license    MIT - see license.txt
 * @version    https://Laegel@bitbucket.org/Laegel/skin.git
 * @link       https://packagist.org/packages/skinengine/skin
 */

namespace SkinEngine;
use \SkinEngine\Translator\Translator,
    \SkinEngine\Parsing\Parser,
    \SkinEngine\Parsing\Replacer,
    \SkinEngine\Cache\Cache,
    \SkinEngine\Exception\ExceptionHandler,
    \SkinEngine\Tools\Tools;

/**
 * Skin class is the link between your app and the package. Many of the functionalities
 * you need are configured from this code.
 * Check http://skin.laegel-developpement.fr/ tutorial if you need some help.
 */
class Skin
{

    private $view, $viewsDir, $samplesDir, $translationsDir, $samplesVariable,
        $cacheDir, $cache, $errorReport, $outputType;

    public static $data, $layer = 'HTML5', $extension = '.skln';

    /**
     * Skin constructor.
     * @param mixed $data
     * @param array $parserSettings
     * @param string $layer
     * @param boolean $toggleCache
     */
    public function __construct($data = null, array $parserSettings = [], $layer = 'HTML5', $toggleCache = true)
    {
        $defaultParserSettings = [
            'viewsDir' => 'views' . DIRECTORY_SEPARATOR,
            'errorReport' => 0,
            'outputType' => 0,
            'samplesDir' => 'samples' . DIRECTORY_SEPARATOR
        ];
        $settings = array_merge($defaultParserSettings, $parserSettings);

        foreach($settings as $key => $setting)
            $this->{$key} = $setting;
        static::$layer = $layer;
        $this->cache = $toggleCache;
        self::$data = json_decode(json_encode($data), true);
    }

    /**
     * Set report level.
     * @param integer $level
     * @return this
     */
    public function setReportLevel($level)
    {
        $this->errorReport = $level;
        return $this;
    }

    /**
     * Set output type.
     * @param integer $type
     * @return this
     */
    public function setOutputType($type)
    {
        $this->outputType = $type;
        return $this;
    }

    /**
     * Toggle cache.
     * @param boolean $value
     * @return this
     */
    public function setToggleCache($value)
    {
        $this->cache = $value;
        return $this;
    }

    /**
     * Set output layer type.
     * @param string $layer
     * @return this
     */
    public static function setLayer($layer)
    {
        static::$layer = $layer;
    }

    /**
     * Set public sample directory.
     * @param string $directory
     * @return this
     */
    public function setSamplesDir($directory)
    {
        $this->samplesDir = $directory;
        return $this;
    }

    /**
     * Set JavaScript sample variable.
     * @param string $variable
     * @return this
     */
    public function setSamplesVariable($variable)
    {
        $this->samplesVariable = $variable;
        return $this;
    }

    /**
     * Set translations directory.
     * @param string $directory
     * @return this
     */
    public function setTranslationsDir($directory)
    {
        $this->translationsDir = $directory;
        return $this;
    }

    /**
     * Set public cache directory.
     * @param string $directory
     * @return this
     */
    public function setCacheDir($directory)
    {
        $this->cacheDir = $directory;
        return $this;
    }

    /**
     * Get view as string.
     * @return string
     */
    public function getContent()
    {
        return $this->view;
    }

    public function partial($partialPath)
    {
        //echo $partialPath;
    }

    /**
     * Parsing and replacement.
     * @param string $path
     * @return string
     */
    private function getRender($path)
    {
        Parser::setViewsDir($this->viewsDir);
        Parser::setSamplesDir($this->samplesDir);
        Parser::setSamplesVariable($this->samplesVariable ?: 'SkinSample');
        Translator::setTranslationsDir($this->translationsDir);
        $structure = Parser::parseContent(
            $path,
            $this->outputType,
            $this->errorReport
        );

        $layer = Tools::getLayer();

        return $layer::beforeTemplate() . Replacer::getOutput($structure) . $layer::afterTemplate();
    }

    /**
     * Render skeleton from path.
     * @param string $path
     * @param boolean $forceExpiry
     */
    function render($path = null, $forceExpiry = false)
    {
        $path = Tools::envPath($path);
        if($this->cache) {
            Cache::setCacheStructure($this->cacheDir);
            Cache::setCacheViewName($path);

            if(!Cache::cacheViewExists() || $forceExpiry) {
                $file = $this->getRender($path);
                Cache::setCacheView($file, $forceExpiry);
            } else
                $file = Cache::getCacheView();
        } else
            $file = $this->getRender($path);
        // $dbg = explode("\n", $file);
        // for($i = 0; $i < count($dbg); ++$i) {
        //     echo $i . '|' . htmlentities($dbg[$i]) . '<br>';
        // }
// file_put_contents('blabliblo.php', $file);
// require 'blabliblo.php';die;
        ob_start();



         eval('$skinLocalVars = [];?>' . $file);

        $this->view = ob_get_clean();
        ExceptionHandler::getExecutionReport($this->errorReport);
        return $this;
    }

    public function output($path = null, $forceExpiry = false)
    {
        $layer = Tools::getLayer();
        if(!empty($layer::$contentType)) {
            header('Content-Type: ' . $layer::$contentType);
        }
        die($this->render($path, $forceExpiry)->view);
    }
}
