<?php
namespace SkinEngine\Elements;

interface LayerInterface
{
    public static function getBooleanAttributes();

    public static function getElements();

    public static function getElementsAlias();

    public static function getGlobalAttributes();

    public static function getPattern();

    public static function getTagStructure();

    public static function invalidAttribute($attribute);

    public static function textElement($element);

    public static function beforeTemplate();

    public static function afterTemplate();

    public static function beforeElement($element, $isLast);

    public static function afterElement($element, $isLast);

    public static function beforeAttribute($attribute, $isLast);

    public static function afterAttribute($attribute, $isLast);
}
