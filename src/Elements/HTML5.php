<?php
namespace SkinEngine\Elements;

class HTML5 extends LayerAbstract
{
    protected static $elements = [
        'doctype' => [
            'selfClosing' => true,
            'required' => true,
            'output' => '<!DOCTYPE html>'
        ],
        'a' => [
            'attributes' => [
                'download', 'href', 'hreflang', 'media', 'rel', 'target', 'type'
            ]
        ],
        'abbr' => [],
        'address' => [],
        'area' => [
            'attributes' => [
                'alt', 'coords', 'download',
                'href', 'hreflang', 'media',
                'rel', 'shape', 'target',
                'type'
            ]
        ],
        'article' => [],
        'aside' => [],
        'audio' => [
            'attributes' => [
                'autoplay', 'controls', 'loop',
                'muted', 'preload', 'src'
            ]
        ],
        'b' => [],
        'base' => [],
        'bdi' => [],
        'bdo' => [],
        'blockquote' => [],
        'body' => [
            'required' => true,
        ],
        'br' => [
            'selfClosing' => true,
            'output' => '<br>'
        ],
        'button' => [
            'attributes' => [
                'autofocus', 'disabled', 'form',
                'formaction', 'formenctype', 'formmethod',
                'formnovalidate', 'formtarget', 'name',
                'type', 'value'
            ]
        ],
        'canvas' => [
            'attributes' => [
                'height', 'width'
            ]
        ],
        'caption' => [
        ],
        'cite' => [
        ],
        'code' => [
        ],
        'col' => [
            'attributes' => [
                'span'
            ]
        ],
        'colgroup' => [
            'attributes' => [
            ]
        ],
        'datalist' => [
        ],
        'dd' => [],
        'del' => [
            'attributes' => [
                'cite', 'datetime'
            ]
        ],
        'details' => [
            'attributes' => [
                'open'
            ]
        ],
        'dfn' => [],
        'dialog' => [
            'attributes' => [
                'open'
            ]
        ],
        'div' => [
            'attributes' => [
                'contenteditable', 'contextmenu'
            ]
        ],
        'dl' => [],
        'dt' => [],
        'em' => [],
        'embed' => [
            'attributes' => [
                'height', 'src', 'type', 'width'
            ]
        ],
        'fieldset' => [
            'attributes' => [
                'disabled', 'form', 'name'
            ]
        ],
        'figcaption' => [],
        'figure' => [],
        'footer' => [],
        'form' => [
            'attributes' => [
                'accept-charset', 'action', 'autocomplete',
                'enctype', 'method', 'name',
                'novalidate', 'target'
            ]
        ],
        'h1' => [],
        'h2' => [],
        'h3' => [],
        'h4' => [],
        'h5' => [],
        'h6' => [],
        'head' => [
            'required' => true,
        ],
        'header' => [],
        'hgroup' => [],
        'hr' => [
            'selfClosing' => true
        ],
        'html' => [
            'required' => true,
            'attributes' => [
                'manifest', 'xmlns'
            ]
        ],
        'i' => [],
        'iframe' => [
            'attributes' => [
                'height', 'name', 'sandbox',
                'seamless', 'src', 'srcdoc',
                'width'
            ]
        ],
        'img' => [
            'selfClosing' => true,
            'attributes' => [
                'alt', 'crossorigin', 'height',
                'ismap', 'src', 'usemap',
                'width'
            ]
        ],
        'input' => [
            'selfClosing' => true,
            'attributes' => [
                'accept', 'alt', 'autocomplete',
                'autofocus', 'checked', 'disabled',
                'form', 'formaction', 'formenctype',
                'formmethod', 'formnovalidate', 'formtarget',
                'height', 'list', 'max',
                'maxlenght', 'min', 'multiple',
                'name', 'pattern', 'placeholder',
                'text', 'readonly', 'required',
                'size', 'src', 'step',
                'type', 'value', 'width'
            ]
        ],
        'ins' => [],
        'kbd' => [],
        'keygen' => [
            'selfClosing' => true,
            'attributes' => [
                'autofocus', 'challenge', 'disabled',
                'form', 'keytype', 'name'
            ]
        ],
        'label' => [
            'attributes' => [
                'for', 'form'
            ]
        ],
        'legend' => [
        ],
        'li' => [
            'attributes' => [
                'value'
            ]
        ],
        'link' => [
            'selfClosing' => true,
            'attributes' => [
                'crossorigin', 'href', 'hreflang',
                'media', 'rel', 'sizes',
                'type'
            ]
        ],
        'main' => [],
        'map' => [],
        'mark' => [],
        'menu' => [],
        'menuitem' => [
            'attributes' => [
                'checked', 'command', 'default',
                'disabled', 'icon', 'label',
                'radiogroup', 'type'
            ]
        ],
        'meta' => [
            'selfClosing' => true,
            'attributes' => [
                'charset', 'content', 'http-equiv',
                'name'
            ]
        ],
        'meter' => [
            'attributes' => [
                'form', 'high', 'low',
                'max', 'min', 'optimum',
                'value'
            ]
        ],
        'nav' => [],
        'noscript' => [],
        'object' => [
            'attributes' => [
                'data', 'form', 'height',
                'name', 'type', 'usemap',
                'width'
            ]
        ],
        'ol' => [
            'attributes' => [
                'reversed', 'start', 'type'
            ]
        ],
        'optgroup' => [
            'attributes' => [
                'disabled', 'label'
            ]
        ],
        'option' => [
            'attributes' => [
                'disabled', 'label', 'selected',
                'value'
            ]
        ],
        'output' => [
            'attributes' => [
                'for', 'form', 'name'
            ]
        ],
        'p' => [
            'attributes' => [
                'contenteditable', 'contextmenu'
            ]
        ],
        'param' => [
            'selfClosing' => true,
            'attributes' => [
                'name', 'value'
            ]
        ],
        'polygon' => [],
        'pre' => [],
        'progress' => [
            'attributes' => [
                'max', 'value'
            ]
        ],
        'q' => [
            'attributes' => [
                'cite'
            ]
        ],
        'rp' => [],
        'rt' => [],
        'ruby' => [],
        's' => [],
        'svg' => [],
        'samp' => [],
        'script' => [
            'attributes' => [
                'async', 'charset', 'defer',
                'src', 'type'
            ]
        ],
        'section' => [],
        'select' => [
            'attributes' => [
                'autofocus', 'disabled', 'form',
                'multiple', 'name', 'required',
                'size'
            ]
        ],
        'small' => [],
        'source' => [
            'selfClosing' => true,
            'attributes' => [
                'media', 'src', 'type'
            ]
        ],
        'span' => [
            'attributes' => [
                'contenteditable', 'contextmenu'
            ]
        ],
        'strong' => [],
        'style' => [
            'attributes' => [
                'media', 'scoped', 'type'
            ]
        ],
        'sub' => [],
        'summary' => [],
        'sup' => [],
        'table' => [
            'attributes' => [
                'sortable'
            ]
        ],
        'tbody' => [],
        'td' => [
            'attributes' => [
                'colspan', 'headers', 'rowspan'
            ]
        ],
        'textarea' => [
            'attributes' => [
                'autofocus', 'cols', 'disabled',
                'form', 'maxlenght', 'name',
                'placeholder', 'readonly', 'required',
                'rows', 'wrap'
            ]
        ],
        'tfoot' => [],
        'th' => [
            'attributes' => [
                'abbr', 'colspan', 'headers',
                'rowspan', 'scope', 'sorted'
            ]
        ],
        'thead' => [],
        'time' => [
            'attributes' => [
                'datetime'
            ]
        ],
        'title' => [
            'required' => true,
        ],
        'tr' => [],
        'track' => [
            'selfClosing' => true,
            'attributes' => [
                'default', 'kind', 'label',
                'src', 'srclang'
            ]
        ],
        'u' => [],
        'ul' => [],
        'var' => [],
        'video' => [
            'attributes' => [
                'autoplay', 'controls', 'height',
                'loop', 'muted', 'poster',
                'preload', 'src', 'width'
            ]
        ],
        'wbr' => []
    ], $contentType = 'text/html';

    public static $specialContainers = [
        'style', 'script'
    ], $format = 'NAME="VALUE"';

    public static function getBooleanAttributes()
    {
        return [
            'allowfullscreen', 'async', 'autofocus', 'autoplay', 'checked',
            'contenteditable', 'controls', 'default',
            'defaultChecked', 'defaultMuted', 'defaultSelected', 'defer',
            'disabled', 'draggable', 'enabled', 'formnovalidate', 'hidden',
            'ismap', 'itemscope', 'loop', 'multiple',
            'muted', 'novalidate', 'open', 'readonly', 'required', 'reversed',
            'scoped', 'seamless', 'selected', 'sortable', 'spellcheck',
            'translate', 'typemustmatch', 'visible'
        ];
    }

    public static function getTagStructure()
    {
        return [
            'selfClosing' => [
                'start' => '<TAG ATTR>'
            ],
            'doubleClosing' => [
                'start' => '<TAG ATTR>',
                'end' => '</TAG>'
            ]
        ];
    }

    public static function getGlobalAttributes()
    {
        return [
            'accesskey', 'class', 'dir', 'draggable', 'dropzone',
            'hidden', 'id', 'itemprop', 'itemscope', 'itemtype', 'lang',
            'spellcheck', 'style', 'tabindex', 'title', 'translate'
        ];
    }

    public static function invalidAttribute($attribute)
    {
        return 0 === strpos($attribute, 'data-') || 0 === strpos($attribute, 'aria-') ? $attribute : 'data-' . $attribute;
    }

    public static function getElements()
    {
        return static::$elements;
    }

    public static function getElementsAlias()
    {
        return [];
    }

    public static function textElement($element)
    {

    }
}
