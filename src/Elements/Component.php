<?php
namespace SkinEngine\Elements;

use \SkinEngine\Skin,
    \SkinEngine\Parsing\Parser,
    \SkinEngine\Parsing\Replacer,
    \SkinEngine\Exception\ExceptionHandler,
    \SkinEngine\Tools\Tools;

class Component
{
    static $path;

    private static $components = [], $current = null;


    public static function register($name, $attributes = [])
    {
        $layer = Tools::getLayer();

        if($layer::create($name, $attributes)) {
            static::$components[$name] = [
                'template' => Tools::fileExtension(static::$path . $name),
            ];
        }
    }


    public static function replaceNode(Element $node)
    {
        static::$current = $node->tag;//Locks the current component to prevent component recursion

        $tree = Parser::parseString(file_get_contents(static::$components[$node->tag]['template']));

        $output = '';

        foreach($tree as $component)
            $output .= static::getOutputNode($node, $component);

        foreach($tree[1] as $name => $property) {
            if('level' !== $name || true) {
                $node->{$name} = $property;
            }
        }
        static::$current = null;
    }


    private static function getOutputNode(Element $node, Element $component)
    {
        $component->level += $node->level;//Add node's level to all component's children

        $countAttributes = count($node->attributes[1]);
        for($i = 0; $i < $countAttributes; ++$i) {

            if(1 === preg_match(Parser::REGEX_VAR_BLOCK, $node->attributes[1][$i], $matchVariable)) {//Is a variable
                $replacement = $matchVariable[2];
            } elseif(1 === preg_match(Parser::REGEX_TRANSLATE_BLOCK, $node->attributes[1][$i], $matchTranslate)) {//Is a translation
                $replacement = $matchTranslate[2];
            } else {//Is just text
                $replacement = $node->attributes[1][$i];
            }

            preg_match('~(?!{\*)' . $node->attributes[0][$i] . '(?!\s*\*})~', $component->input, $match);
            preg_match('~{\*\s*' . $node->attributes[0][$i] . '\s*\*}~', $component->input, $matchText);

            if(!empty($matchText)) {//replaces {*ATTR*} into simple text
                $component->start = str_replace($matchText[0], $replacement, $component->start);
                $component->input = str_replace($matchText[0], $replacement, $component->input);
            }

            if(!empty($match)) {
                if(!empty($component->variables)) {
                    foreach($component->variables as $variable) {
                        $component->start = str_replace($variable['input'], str_replace($match[0], $replacement, $variable['input']), $component->start);
                        $component->input = str_replace($variable['input'], str_replace($match[0], $replacement, $variable['input']), $component->input);
                    }
                }

                if(!empty($component->translations)) {
                    foreach($component->translations as $translation) {
                        $component->start = str_replace($translation['input'], str_replace($match[0], $replacement, $translation['input']), $component->start);
                        $component->input = str_replace($translation['input'], str_replace($match[0], $replacement, $translation['input']), $component->input);
                    }
                }
                //Browse variables and translations to get input and replace start match
                Parser::prepareOutputNode($component);
                // $component->clear();
                $component->variables = [];
                $component->translations = [];
                // $component->attributes = [];
                // $component->loop = [];

            }


        }



        if(isset($component->children) && !empty($component->children)) {
            foreach($component->children as $child) {
                static::getOutputNode($node, $child);
            }
        }
    }

    public static function exists($name)
    {
        return isset(static::$components[$name]);
    }

    public static function getCurrent()
    {
        return static::$current;
    }
}
