<?php
namespace SkinEngine\Elements;

use \SkinEngine\Parsing\Parser,
    \SkinEngine\Exception\ExceptionHandler,
    \SkinEngine\Skin,
    \SkinEngine\Tools\Tools,
    \SkinEngine\Elements\Component;

class Element
{
    public  $tag, $line, $input,
            $start, $end, $parent,
            $attributes = [], $booleanAttributes = [], $type,
            $loop, $condition,
            $variables = [], $translations = [],
            $level = 0, $children = [], $toParse = true,
            $selfClosing = false;

    const   ELEMENT_TYPE_TAG = 0, ELEMENT_TYPE_TEXT = 1, ELEMENT_TYPE_COMPO = 2;

    /**
     * SkinElement constructor.
     * @param integer $line
     * @param string $input
     */
    public function __construct($line, $input)
    {
        $this->line = $line;
        $this->input = str_replace("\t", '    ', $input);//Changes four whitespaces into a tab
        $this->setTag(Parser::parseTag(trim($this->input)));

        if(!$this->isText()) {
            $this->setAttributes(Parser::parseAttributes($this));
            //If tag has no attribute but input has many chars, it is a string
            if(empty($this->attributes[0]) && trim($this->input) !== $this->tag) {
                $this->type = static::ELEMENT_TYPE_TEXT;
                $this->tag = '';
                $this->start = '';
                $this->end = '';
            }
        }

        $this->setLevel();
        $this->input = trim($this->input);
        if($this->isComponent()) {
            if($this->tag === Component::getCurrent()) {
                ExceptionHandler::addError(
                    ExceptionHandler::E_RECURSIVE_COMPONENT, ['FILE' => Tools::fileExtension($this->tag)]
                );
            } else {
                Component::replaceNode($this);
            }
        }
    }

    /**
     * Add a parsed variable to current node.
     * @param string $input
     * @param array $variable
     * @param array $functions
     */
    function addVariable($input, array $variable, array $functions = [])
    {
        $variable['functions'] = $functions;
        $variable['input'] = $input;
        array_push($this->variables, $variable);
    }

    /**
     * Add a parsed translation to current node.
     * @param array $translation
     */
    function addTranslation(array $translation)
    {
        array_push($this->translations, $translation);
    }

    /**
     * Add a parsed boolean attribute to current node.
     * @param array $attribute
     */
    function addBooleanAttribute(array $attribute)
    {
        array_push($this->booleanAttributes, $attribute);
    }

    /**
     * Set the parsed condition to current node.
     * @param array $condition
     * @param boolean $type
     */
    function setCondition(array $condition, $type)
    {
        $condition['conditionType'] = $type;
        $this->condition = $condition;
    }

    /**
     * Set the parsed loop to current node.
     * @param array $loop
     * @param string $alias
     */
    function setLoop(array $loop, $alias)
    {
        $loop['alias'] = $alias;
        $this->loop = $loop;
    }

    /**
     * Set the node level from the indent in skeleton.
     */
    private function setLevel()
    {
        preg_match('~[\s]*~', $this->input, $match);
        $this->level = strlen($match[0]);
    }

    /**
     * Replace node format by attributes.
     * @param string $format
     * @param string $name
     * @param string $value
     * @return string
     */
    private function associate($format, $name, $value)
    {
        if ($name !== 'style')
            $value = str_replace(';', ' ', $value);
        return str_replace(['NAME', 'VALUE'], [$name, preg_replace('~\s+~', ' ', $value)], $format);
    }

    private function injectClass($className)
    {
        $this->attributes;
    }

    /**
     * Set node structure from passed tag.
     * @param string $tag
     */
    private function setTag($tag)
    {
        $skinLayer = Tools::getLayer();
        $elements = Tools::arrayToObject($skinLayer::getElements());
        $elementsAlias = Tools::arrayToObject($skinLayer::getElementsAlias());

        if (isset($elements->{$tag}) || $skinLayer::isPermissive()) {
            if(Component::exists($tag)) {//If tag matches a registered component
                $this->type = static::ELEMENT_TYPE_COMPO;
            }
            if (isset($elements->{$tag}->output)) {//If the output is already defined
                $tags = Tools::arrayToObject($skinLayer::getTagStructure());

                if (!isset($elements->{$tag}->selfClosing) || !$elements->{$tag}->selfClosing) {
                    $format = $tags->doubleClosing;
                    $this->end = str_replace('TAG', $tag, $format->end);
                } else {
                    $this->selfClosing = true;
                }
                $this->start = $elements->{$tag}->output;
            } else {//We get the tag structure and we replace it with its name and attributes

                $tags = Tools::arrayToObject($skinLayer::getTagStructure());
                if (isset($elements->{$tag}->selfClosing) && $elements->{$tag}->selfClosing) {
                    $this->selfClosing = true;
                    $format = $tags->selfClosing;
                } else {
                    $this->selfClosing = false;
                    $format = $tags->doubleClosing;
                    $this->end = str_replace('TAG', $tag, $format->end);
                }
                $this->start = str_replace('TAG', $tag, $format->start);
            }
            Parser::addParsedTag($tag);
        } elseif (isset($elementsAlias->{$tag})) {//For alias tags (allows retrocompatibility between versions)
            if (isset($elementsAlias->{$tag}->output))//If the output is already defined
                $this->start = $elementsAlias->{$tag}->output;
            else {
                $this->end = isset($elementsAlias->{$tag}->end) ? $elementsAlias->{$tag}->end : '';
                $this->start = $elementsAlias->{$tag}->start;
            }
            $this->dataInject = $tag;
        } elseif(preg_match('~^' . $skinLayer::getPattern() . '$~', $tag, $match)) {
            $format = Tools::arrayToObject($skinLayer::getTagStructure())->doubleClosing;
            $this->selfClosing = false;
            $tag = $match[0];
            $this->end = str_replace('TAG', $tag, $format->end);
            $this->start = str_replace('TAG', $tag, $format->start);
            Parser::addParsedTag($tag);
        } else {
            $this->type = static::ELEMENT_TYPE_TEXT;
            $tag = '';
            $this->toParse = false;
            $this->start = '';
        }
        $this->tag = $tag;
    }

    /**
     * Set node attributes from passed attributes.
     * @param array $attributes
     */
    private function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function replaceAttributes()
    {
        $attributes = $this->attributes;
        if (!empty($attributes)) {
            $skinLayer = Tools::getLayer();

            $globalAttributes = $skinLayer::getGlobalAttributes();
            $booleanAttributes = $skinLayer::getBooleanAttributes();
            $elements = $skinLayer::getElements();

            $attributesOutput = '';
            $countAttributes = count($attributes[0]);
            if (isset($this->dataInject) && !in_array('class', $attributes[0])) {
                array_push($attributes[0], 'class');
                array_push($attributes[1], $this->dataInject);
            }

            for ($i = 0; $i < $countAttributes; ++$i) {

                $attributes[0][$i] = strtolower($attributes[0][$i]);
                if (!in_array($attributes[0][$i], Parser::$skinAttributes)) {
                    if (isset($this->dataInject) && 'class' === $attributes[0][$i])
                        $attributes[1][$i] = $this->dataInject . ';' . $attributes[1][$i];

                    if (!$this->isText() && !in_array($attributes[0][$i], $globalAttributes) && !(isset($elements[$this->tag]['attributes']) && in_array($attributes[0][$i], $elements[$this->tag]['attributes']))) {
                        $temp = str_replace('is', '', $attributes[0][$i]);//Boolean attributes
                        if(0 === strpos($attributes[0][$i], 'is') && in_array($temp, $booleanAttributes)) {
                            $exploded = explode(',', $attributes[1][$i]);
                            $countExploded = count($exploded);
                            $expressions = [];
                            foreach($exploded as $expression) {
                                preg_match_all(Parser::REGEX_VAR_BLOCK, $expression, $matchVariable);
                                if(!empty($matchVariable[2][0])) {
                                    $expressions[] = Parser::parseProperty($matchVariable[2][0]);
                                } else {
                                    $expressions[] = $expression;
                                }
                            }

                            $attributesOutput .= 'BOOL_ATTR_' . strtoupper($temp) . ' ';
                            $this->addBooleanAttribute([
                                'attribute' => $temp, 'hook' => 'BOOL_ATTR_' . strtoupper($temp),
                                'expressions' => $expressions
                            ]);
                            continue;
                        } elseif(method_exists($skinLayer, 'invalidAttribute')) {//Adapt an invalid attribute
                            $adaptedAttribute = $skinLayer::invalidAttribute($attributes[0][$i]);
                            if($adaptedAttribute !== $attributes[0][$i]) {
                                ExceptionHandler::addWarning(
                                    ExceptionHandler::W_ATTRIBUTE_ADAPTED,
                                    ['ATTRIBUTE' => $attributes[0][$i], 'ADAPTED' => $adaptedAttribute, 'ELEMENT' => $this->tag, 'LINE' => Parser::$currentLine]
                                );
                                $attributes[0][$i] = $adaptedAttribute;
                            }
                        } else {//Remove invalid attribute
                            ExceptionHandler::addWarning(
                                ExceptionHandler::W_ATTRIBUTE_INVALID,
                                ['ATTRIBUTE' => $attributes[0][$i], 'ELEMENT' => $this->tag, 'LINE' => Parser::$currentLine]
                            );
                            continue;
                        }
                    }
                    $attributesOutput .= $this->associate($skinLayer::$format, $attributes[0][$i], $attributes[1][$i]) . (1 + $i === $countAttributes ? '' : ' ');
                }
            }
            $this->start = str_replace('ATTR', $attributesOutput, $this->start);
        } else
            $this->start = str_replace(' ATTR', '', $this->start);
    }

    public function setOutputLevel()
    {
        if(1 === Parser::$outputMode) {
            $hasChildren = $this->hasChildren();
            $tabs = str_repeat(' ', $this->level);
            $this->start = $tabs . $this->start . ($hasChildren || $this->selfClosing || $this->isText() ? PHP_EOL : '');
            if(!empty($this->end))
                $this->end = ($hasChildren ? $tabs : '') . $this->end . PHP_EOL;
        }
    }

    public function clear()
    {
        $this->translations = [];
        $this->variables = [];
        $this->attributes = [];
        $this->loop = [];
    }

    public function isText()
    {
        return static::ELEMENT_TYPE_TEXT === $this->type;
    }

    public function isComponent()
    {
        return static::ELEMENT_TYPE_COMPO === $this->type;
    }

    public function hasChildren()
    {
        return !empty($this->children);
    }

    public function addAttribute($key, $value)
    {
        $match = false;
        foreach($this->attributes[0] as $index => $attribute) {
            if($key === $attribute) {
                $this->attributes[1][$index] = $key;
                ExceptionHandler::addWarning(
                    ExceptionHandler::W_ATTRIBUTE_OVERRIDE,
                    ['ATTRIBUTE' => $this->attributes[0][$i], 'ELEMENT' => $this->tag]
                );
                $match = true;
                break;
            }
        }
        if(!$match) {
            $this->attributes[0][] = $key;
            $this->attributes[1][] = $value;
        }
        $this->setTag($this->tag);
        $this->setAttributes($this->attributes);
        $this->replaceAttributes();
        $this->setOutputLevel();
    }

    public function removeAttribute($key)
    {
        foreach($this->attributes[0] as $index => $attribute) {
            if($key === $attribute) {
                unset($this->attributes[0][$index]);
                unset($this->attributes[1][$index]);
                $this->setTag($this->tag);
                $this->setAttributes([array_values($this->attributes[0]), array_values($this->attributes[1])]);
                $this->replaceAttributes();
                $this->setOutputLevel();
                break;
            }
        }
    }
}
