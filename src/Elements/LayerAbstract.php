<?php
namespace SkinEngine\Elements;
use \SkinEngine\Skin;

abstract class LayerAbstract implements LayerInterface
{
    public static $specialContainers = [];

    protected static $isPermissive = false, $elements = [], $contentType;

    public static function getPattern()
    {
        return '[ ]';
    }

    public static function create($name, array $options = [])
    {
        if(!isset(static::$elements[$name])) {
            static::$elements[$name] = $options;
            return true;
        } else {
            ExceptionHandler::addError(
                ExceptionHandler::E_DUPLICATE_ELEMENT,
                [
                    'TAG' => $name
                ]
            );
            return false;
        }
    }

    public static function update($name, array $options)
    {
        if(isset(static::$elements[$name])) {
            static::$elements[$name] = $options;
        }
    }

    public static function isPermissive()
    {
        return static::$isPermissive;
    }

    public static function beforeTemplate()
    {
        return '';
    }

    public static function afterTemplate()
    {
        return '';
    }

    public static function beforeElement($element, $isLast)
    {
        return '';
    }

    public static function afterElement($element, $isLast)
    {
        return '';
    }

    public static function beforeAttribute($attribute, $isLast)
    {
        return '';
    }

    public static function afterAttribute($attribute, $isLast)
    {
        return '';
    }
}
