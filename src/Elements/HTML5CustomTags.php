<?php
namespace SkinEngine\Elements;

class HTML5CustomTags extends HTML5
{
    static function getPattern()
    {
        return '[a-zA-Z]+(?:-[a-zA-Z]+)+';
    }
}
