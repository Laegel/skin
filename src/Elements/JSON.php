<?php
namespace SkinEngine\Elements;

class JSON extends LayerAbstract
{
    protected static $isPermissive = true, $contentType = 'application/json';

    public static $format = '"NAME": "VALUE"';

    public static function getTagStructure()
    {
        return [
            'selfClosing' => [
                'start' => '"TAG": {
                    ATTR'
            ],
            'doubleClosing' => [
                'start' => '"TAG": {
                    ATTR',
                'end' => '}'
            ]
        ];
    }

    public static function invalidAttribute($attribute)
    {
        return $attribute;
    }

    public static function getElements()
    {
        return static::$elements;
    }

    public static function getElementsAlias()
    {
        return [];
    }

    public static function getBooleanAttributes()
    {
        return [];
    }

    public static function getGlobalAttributes()
    {
        return [];
    }

    public static function textElement($element)
    {
        return '"' . $element . '"';
    }

    public static function beforeTemplate()
    {
        return '{';
    }

    public static function afterTemplate()
    {
        return '}';
    }

    public static function beforeElement($element, $isLast)
    {

    }

    public static function afterElement($element, $isLast)
    {
        if(!$isLast) {
            $element .= ',';
        }
    }

    public static function beforeAttribute($attribute, $isLast)
    {

    }

    public static function afterAttribute($attribute, $isLast)
    {
        if(!$isLast) {
            $attribute .= ',';
        }
    }
}
