<?php
namespace SkinEngine\Elements;

class HTML6 extends HTML5
{
    static function getPattern()
    {
        return '[a-zA-Z]+(?::[a-zA-Z]+)+';
    }
}
