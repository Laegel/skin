<?php
namespace SkinEngine\Tools;
use \SkinEngine\Translator\Translator,
    \SkinEngine\Tools\Compare,
    \SkinEngine\Parsing\Parser,
    \SkinEngine\Parsing\Replacer,
    \SkinEngine\Tools\Group;

class Functions
{
    static $current = [];
    /*
     * Once skeleton template is converted to PHP, it uses those methods.
     */

    /**
     * Output data.
     * @param array $context
     * @param string $index
     * @param array $treatment
     */
    final static function output($context, $index, $treatment = [])
    {
        $class = get_called_class();
        $escape = true;
        $output = isset($context[$index]) ? $context[$index] : '';
        if(!empty($treatment)) {
            $countTreatment = count($treatment);
            for($i = 0; $i < $countTreatment; ++$i) {
                $output = Group::execute($output, $treatment[$i]);
            }
            if(in_array('ue', $treatment) || in_array('unescape', $treatment))
                $escape = false;
        }
        print $escape ? self::escape($output) : $output;
    }

    /**
     * Convert any data to its boolean equivalent.
     * @param mixed $data
     * @return boolean
     */
    final static function toBoolean($data)
    {
        if(!is_string($data))
            return (bool) $data;
        switch(strtolower($data)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'y':
            default:
                return true;
            case '0':
            case 'false':
            case 'off':
            case 'no':
            case 'n':
                return false;
        }
    }

    /**
     * Translate a string.
     * @param string $index
     * @param array $variables
     */
    final static function translate($index, $variables = [])
    {
        $structure = Parser::parseString(Translator::translate($index, $variables));
        print Replacer::getOutput($structure);
    }

    final static function prepareItemPlain($data)
    {
        return is_array($data) ? '' : $data;
    }

    /**
     *
     * @param  [type] $data      [description]
     * @param  [type] $index     [description]
     * @param  [type] $attribute [description]
     * @return [type]            [description]
     */
    final static function compare($data, $attribute)
    {
        if(is_array($data)) {
            $counted = count($data);

            for($i = 0; $i < $counted; ++$i) {
                if($i !== 0 && (string)$data[$i - 1] !== $data[$i]) {
                    return;
                }
            }

        } elseif(!static::toBoolean($data)) {
            return;
        }
        $layer = Tools::getLayer();

        print str_replace(['NAME', 'VALUE'], $attribute, $layer::$format);
    }

    /*Treatment functions*/
    protected static function addCurrency($price)
    {
        return Translator::$languagesSettings[Translator::getLanguage()]['afterSymbol'] ?
            Translator::$languagesSettings[Translator::getLanguage()]['currencySymbol'] . $price :
            $price . Translator::$languagesSettings[Translator::getLanguage()]['currencySymbol'];
    }

    protected static function capitalize($string)
    {
        return ucwords($string);
    }

    protected static function dateFormat($date)
    {
        $format = Translator::$languagesSettings[Translator::getLanguage()]['dateFormat'];
        $check = \DateTime::createFromFormat($format, $date);
        if($check && $check->format($format) === $date) {
            return $date;
        } else {
            $dateObject = new \DateTime($date);
            return $dateObject->format($format);
        }
    }

    protected static function decrease($int = 0)
    {
        return is_numeric($int) ? --$int : $int;
    }

    protected static function escape($string = '')
    {
        return htmlentities($string);
    }

    protected static function increase($int = 0)
    {
        return is_numeric($int) ? ++$int : $int;
    }

    protected static function lowercase($string)
    {
        return strtolower($string);
    }

    protected static function minimize($string)
    {
        return strtolower(self::stripAccents($string));
    }

    protected static function moneyFormat($price)
    {
        return self::addCurrency(self::numberFormat($price));
    }

    protected static function numberFormat($number)
    {
        return number_format($number, 2,
            Translator::$languagesSettings[Translator::getLanguage()]['numberFormat']['decimalSeparator'],
            Translator::$languagesSettings[Translator::getLanguage()]['numberFormat']['thousandSeparator']
        );
    }


    protected static function romanicNumber($integer)
    {
        $values = [
            'M' => 1000, 'CM' => 900, 'D' => 500,
            'CD' => 400, 'C' => 100, 'XC' => 90,
            'L' => 50, 'XL' => 40, 'X' => 10,
            'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1
        ];
        $romanic = '';
        while(0 < $integer) {
            foreach($values as $key => $value) {
                if($integer >= $value) {
                    $integer -= $value;
                    $romanic .= $key;
                    break;
                }
            }
        }
        return $romanic;
    }

    protected static function stripAccents($string)
    {
        return str_replace(
            array(
                'à', 'á', 'â', 'ã', 'ä', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î',
                'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ',
                'À', 'Á', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î',
                'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý'
            ),
            array(
                'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i',
                'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y',
                'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I',
                'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y'
            ),
            $string
        );
    }

    protected static function truncate($string)
    {
        $maxLength = static::$current['arguments'][0] ?: 30;
        $subString = substr($string, 0, $maxLength);
        return strlen($string) > $maxLength ? $subString . '...' : $subString;
    }

    protected static function unescape($string)
    {
        return $string;
    }

    protected static function uppercase($string)
    {
        return strtoupper($string);
    }

    /*
      ALIASING - SHORTCUTS
     */

    protected static function ac($price)
    {
        return static::addCurrency($price);
    }

    protected static function c($string)
    {
        return static::capitalize($string);
    }

    protected static function df($date)
    {
        return static::dateFormat($date);
    }

    protected static function e($string)
    {
        return static::escape($string);
    }

    protected static function l($string)
    {
        return static::lowercase($string);
    }

    protected static function m($string)
    {
        return static::minimize($string);
    }

    protected static function mf($price)
    {
        return static::moneyFormat($price);
    }

    protected static function nf($number)
    {
        return static::numberFormat($number);
    }

    protected static function rn($integer)
    {
        return static::romanicNumber($integer);
    }

    protected static function sa($string)
    {
        return static::stripAccents($string);
    }

    protected static function t($string)
    {
        return static::truncate($string);
    }

    protected static function ue($string)
    {
        return $string;
    }

    protected static function u($string)
    {
        return static::uppercase($string);
    }
}
