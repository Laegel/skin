<?php
namespace SkinEngine\Tools;

use \SkinEngine\Skin;

class Tools
{
    public static function fileExtension($string)
    {
        if(false === strpos($string, Skin::$extension)) {//If no extension
            return $string . Skin::$extension;
        }
        return $string;
    }

    public static function arrayToObject($array)
    {
        return json_decode(json_encode($array));
    }

    public static function envPath($path)
    {
        return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    }

    public static function getLayer()
    {
        return 'SkinEngine\Elements\\' . Skin::$layer;
    }

    public static function sanitize($string)
    {
        return addslashes(trim($string));
    }
}
