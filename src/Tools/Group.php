<?php
namespace SkinEngine\Tools;

class Group
{
    private static $groups = [];

    /**
     * Register a group of treatment to apply to variables.
     * @param  string $name
     * @param  array  $callbacks
     */
    public static function register($name, array $callbacks)
    {
        static::$groups[$name] = $callbacks;
    }

    private static function callback($name, $params)
    {
        return call_user_func_array($name, $params);
    }

    public static function exists($name)
    {
        return isset(static::$groups[$name]);
    }

    /**
     * Execute a group of treatment to a variable.
     * @param  string $variable
     * @param  string $name
     * @return string
     */
    public static function execute($variable, $name)
    {
        if(isset(static::$groups[$name])) {
            if(!empty(static::$groups[$name])) {
                foreach(static::$groups[$name] as $key => $value) {
                    if(is_array($value)) {
                        array_unshift($value, $variable);
                        $variable = static::callback($key, $value);
                    } else {
                        $variable = static::callback($value, [$variable]);
                    }
                }
            }
        }
        return $variable;
    }
}
