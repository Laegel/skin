<?php
namespace SkinEngine\Translator;

use \SkinEngine\Exception\ExceptionHandler;

class Translator
{
    private static $translations = [], $translationsDir, $language = 'fr_FR', $dataCallback = null;
    public static $languagesSettings = [];

    static function setDataCallback($callback)
    {
        self::$dataCallback = is_callable($callback) ? $callback : null;
    }

    static function setLanguageSettings($language, array $settings)
    {
        self::$languagesSettings[$language] = $settings;
    }

    static function setLanguage($language)
    {
        self::$language = isset(self::$languagesSettings[$language]) ? $language : 'fr_FR';
    }

    static function setTranslationsDir($directory = 'translations')
    {
        self::$translationsDir = $directory;
    }

    private static function setTranslations()
    {
        if(empty(self::$translations)) {
            if(file_exists(self::$translationsDir . self::$language . '.php')) {
                self::$translations = isset(self::$dataCallback) ?
                    call_user_func(self::$dataCallback, self::$translationsDir . self::$language) :
                    require self::$translationsDir . self::$language . '.php';
            } else {
                ExceptionHandler::addError(ExceptionHandler::E_TRANSLATE_FILE_MISSING, [
                    'FILE' => self::$translationsDir . self::$language . '.php'
                ]);
            }
        }
    }

    private static function checkTranslation($index, $number = 0)
    {
        if(!isset(self::$translations['translations'][$index])) {
            ExceptionHandler::addWarning(ExceptionHandler::W_TRANSLATION_MISSING, [
                'INDEX' => $index
            ]);
            return '';
        }
        if(is_array(self::$translations['translations'][$index])) {
            if((bool)count(array_filter(array_keys(self::$translations['translations'][$index]), 'is_string'))) {
                $inflections = self::$languagesSettings[self::$language]['inflections'];
                $determined = 'other';
                foreach($inflections as $key => $inflection) {
                    $tempNumber = is_float($number) && $number >= self::$languagesSettings[self::$language]['plural'] ? ceil($number) : floor($number);
                    if(in_array($tempNumber, $inflection)) {
                        $determined = $key;
                        break;
                    }
                }
                $return = self::$translations['translations'][$index][$determined];
            } else //Get random translation
                $return = self::$translations['translations'][$index][rand(0, count(self::$translations['translations'][$index]) - 1)];
        } else
            $return = self::$translations['translations'][$index];
        return $return;
    }

    static function getList($index)
    {
        self::setTranslations();
        return self::$translations['lists'][$index];
    }

    static function translate($index, array $params = [])
    {
        self::setTranslations();
        if(!empty($params)) {
            array_unshift($params, self::checkTranslation($index, $params[0]));
            return call_user_func_array('sprintf', $params);
        } else {
            return self::checkTranslation($index);
        }
    }

    static function t($index, array $params = [])
    {
        return self::translate($index, $params);
    }

    static function _t($index, array $params = [])
    {
        return self::translate($index, $params);
    }

    static function _l($index)
    {
        return self::getList($index);
    }

    static function getLanguage()
    {
        return self::$language;
    }
}
