<?php
namespace SkinEngine\Translator;

use \SkinEngine\Translator\Translator;

class Language
{
    private static $languages = [
        'fr_FR' => [
            'inflections' => ['one' => [0, 1]],
            'plural' => 2,
            'dateFormat' => 'd/m/Y',
            'currencySymbol' => '€',
            'afterSymbol' => true,
            'numberFormat' => [
                'decimalSeparator' => ',',
                'thousandSeparator' => ' '
            ]
        ],
    	'en_GB' => [
    		'inflections' => ['zero' => [0], 'one' => [1]],
    		'plural' => 1,
    		'dateFormat' => 'd/m/Y',
    		'currencySymbol' => '£',
    		'afterSymbol' => false,
    		'numberFormat' => [
    			'decimalSeparator' => '.',
    			'thousandSeparator' => ','
    		]
    	],
    	'en_US' => [
    		'inflections' => ['zero' => [0], 'one' => [1]],
    		'plural' => 1,
    		'dateFormat' => 'n/j/Y',
    		'currencySymbol' => '$',
    		'afterSymbol' => false,
    		'numberFormat' => [
    			'decimalSeparator' => '.',
    			'thousandSeparator' => ','
    		]
    	],
    	'ru_RU' => [
    		'inflections' => ['zero' => [0], 'one' => [1], 'few' => [2, 4]],
    		'dateFormat' => 'd.m.Y',
    		'currencySymbol' => '₽',
    		'afterSymbol' => true,
    		'numberFormat' => [
    			'decimalSeparator' => ',',
    			'thousandSeparator' => ' '
    		]
    	]
    ];

    public static function create($locale, array $inflections, $plural, $dateFormat, $currencySymbol, $afterSymbol, array $numberFormat)
    {
        self::$languages[$locale] = [
            'inflections' => $inflections,
    		'plural' => $plural,
    		'dateFormat' => $dateFormat,
    		'currencySymbol' => $currencySymbol,
    		'afterSymbol' => $afterSymbol,
    		'numberFormat' => $numberFormat
        ];
    }

    public static function registerLanguage($language)
    {
        if(isset(self::$languages[$language]))
            Translator::setLanguageSettings($language, self::$languages[$language]);
    }

    public static function registerAll()
    {
        foreach(self::$languages as $key => $value) {
            Translator::setLanguageSettings($key, $value);
        }
    }

}
