<?php
namespace SkinEngine\Parsing;

use \SkinEngine\Elements\Element,
    \SkinEngine\Parsing\Parser,
    \SkinEngine\Tools\Tools;

class Replacer
{
    public static $contexts = [], $dataContext = [],
        $specialProperties = [
            '$' => '\'SKIN_LOOP_COUNT\'', '#' => '\'SKIN_LOOP_INDEX\'', '_' => '\'SKIN_ITEM_PLAIN\''
        ], $functionClass = 'Functions';

    private static $walker;

    /**
     * Walk tree of nodes.
     * @param array $tree
     * @return string
     */
    public static function getOutput(array $tree)
    {
        $output = '';
        foreach($tree as $node)
            $output .= static::getOutputNode($node);
        return $output;
    }

    public static function setWalker($callback)
    {
        if(is_callable($callback)) {
            static::$walker = $callback;
        } else {
            //Warning
        }
    }

    /**
     * Creates the output from the current node.
     * @param Element $node
     * @return string
     */
    private static function getOutputNode(Element $node)
    {
        $content = static::replaceDataContext($node);
        if(!empty(static::$walker)) {
            call_user_func_array(static::$walker, [$node]);
        }


        static::replaceConditionBlock($node);


        if(!empty($node->loop)) {
            $test = static::replaceLoopBlock($node);
            // output($test);

            $content = static::replaceBooleanAttributeHooks($node, static::replaceTranslationBlocks($node, static::replaceVariableBlocks($node, $test)));
            // output($test);

        } else {
            // if(!empty($node->loop)) {
            //     $content = static::replaceLoopBlock($node);
            // } else
            //     foreach($node->children as $child)
            //         $content .= static::getOutputNode($child);
            if(isset($node->children) && !empty($node->children))
                foreach($node->children as $child)
                    $content .= static::getOutputNode($child);
            $content = static::replaceBooleanAttributeHooks($node, static::replaceTranslationBlocks($node, static::replaceVariableBlocks($node, $node->start . $content . $node->end)));
        }


        return $content;
    }

    /**
     * Binds data to initialize in special contexts (loops).
     * @param type $context
     * @param array $data
     */
    static function addDataContext($context, array $data)
    {
        if(!isset(static::$dataContext[$context]))
            static::$dataContext[$context] = [$data];
        else
            array_push(static::$dataContext[$context], $data);
    }


    /**
     * Inject internal data into the template.
     * @param Element $node
     */
    private static function replaceDataContext(Element $node)
    {
        $context = empty(static::$contexts) ? 'GLOBAL' : end(static::$contexts);
        if(isset(static::$dataContext[$context])) {
            $dataContext = '<?php ';
            $countData = count(static::$dataContext[$context]);
            for($i = 0; $i < $countData; ++$i)
                $dataContext .= '$skinLocalVars[\'' . static::$dataContext[$context][$i]['index'] . '\'] = [];';
            $dataContext .= '?>' . PHP_EOL;
            unset(static::$dataContext[$context]);
            $node->start = $dataContext . $node->start;
        }
    }

    /**
     * Replace variable blocks by PHP code.
     * @param Element $node
     * @param type $code
     * @return type
     */
    private static function replaceVariableBlocks(Element $node, $code)
    {
        $countVariables = count($node->variables);

        for($i = 0; $i < $countVariables; ++$i) {
            $property = '(isset(' . $node->variables[$i]['context'] . ') ? ' . $node->variables[$i]['context'] . ' : NULL), ' . Parser::prepareSubVariableBlocks($node->variables[$i]['property']);

            $parsedInput = '<?php SkinEngine\Tools\Functions::$current[\'target\'] = [\'context\' => (isset(' . $node->variables[$i]['context'] . ') ? ' . $node->variables[$i]['context'] . ' : NULL), \'property\' => \'' . $node->variables[$i]['property'] . '\'];';
            $parsedInput .= 'SkinEngine\Tools\Functions::$current[\'arguments\'] = [';

            $string = 'SkinEngine\Tools\Functions::output(';
            $treatments = '';
            $parsedFunction = '';
            if(!empty($node->variables[$i]['functions'])) {
                $countFunctions = count($node->variables[$i]['functions']);
                $treatments .= ', [';
                for($j = 0; $j < $countFunctions; ++$j) {
                    $treatments .= '\'' . $node->variables[$i]['functions'][$j]['function'] . '\'' . ($j + 1 === $countFunctions ? '' : ', ' );
                    $parsedFunction = '\'' . implode('\', \'', $node->variables[$i]['functions'][$j]['arguments']) . '\'';
                }

                $treatments .= ']';
            }
            $parsedInput .= $parsedFunction . '];';

            if(isset($node->variables[$i]['property']) && isset(static::$specialProperties[$node->variables[$i]['property']])) {
                $property = '$skinLocalVars[\'$' . $node->variables[$i]['inputParsed'][0] . '\'], ' . static::$specialProperties[$node->variables[$i]['property']];
            }

            $string .= $property . $treatments . ');?>';
            $code = str_replace($node->variables[$i]['input'], $parsedInput . $string, $code);
        }
        return $code;
    }

    /**
     * Replace translation blocks by PHP code.
     * @param \Element $node
     * @param type $code
     * @return type
     */
    private static function replaceTranslationBlocks(Element $node, $code)
    {
        $countTranslations = count($node->translations);
        for($i = 0; $i < $countTranslations; ++$i) {
            $countArguments = count($node->translations[$i]['variables']);
            if(0 === $countArguments) {
                $variables = '';
            } else {
                $variables = ', [';
                for($j = 0; $j < $countArguments; ++$j) {

                    $temp = isset(static::$specialProperties[$node->translations[$i]['variables'][$j]['property']]) ?
                        '$skinLocalVars[\'$' . $node->translations[$i]['variables'][$j]['inputParsed'][0] . '\'][' . static::$specialProperties[$node->translations[$i]['variables'][$j]['property']] . ']' :
                        $node->translations[$i]['variables'][$j]['context'] . '[\'' . $node->translations[$i]['variables'][$j]['property'] . '\']';

                    $variables .= $temp . ($j + 1 === $countArguments ? '' : ', ');
                    //$variables .= $node->translations[$i]['variables'][$j]['context'] . '[\'' . $node->translations[$i]['variables'][$j]['property'] . '\']' . ($j + 1 === $countArguments ? '' : ', ');

                }

                $variables .= ']';
            }
            $code = str_replace($node->translations[$i]['input'], '<?php SkinEngine\Tools\Functions::translate(\'' . $node->translations[$i]['index'] . '\'' . $variables . ');?>', $code);
        }
        return $code;
    }

    private static function replaceBooleanAttributeHooks(Element $node, $code)
    {
        $countBooleanAttributes = count($node->booleanAttributes);
        for($i = 0; $i < $countBooleanAttributes; ++$i) {

            if(1 === count($node->booleanAttributes[$i]['expressions'])) {
                $expression = $node->booleanAttributes[$i]['expressions'][0];
                if(is_array($expression)) {
                    $property = isset(static::$specialProperties[$expression['property']]) ?
                        '$skinLocalVars[\'$' . $expression['inputParsed'][0] . '\'][' . static::$specialProperties[$expression['property']] . ']' :
                        $expression['context'] . '[\'' . $expression['property'] . '\']';
                } else {
                    $property =  '\'' . Tools::sanitize($expression) . '\'';
                }
            } else {
                $property = '[';
                end($node->booleanAttributes[$i]['expressions']);
                $last = key($node->booleanAttributes[$i]['expressions']);
                foreach($node->booleanAttributes[$i]['expressions'] as $key => $expression) {
                    if(is_array($expression)) {
                        $property .= isset(static::$specialProperties[$expression['property']]) ?
                            '$skinLocalVars[\'$' . $expression['inputParsed'][0] . '\'][' . static::$specialProperties[$expression['property']] . ']' :
                            $expression['context'] . '[\'' . $expression['property'] . '\']';
                    } else {
                        $property .=  '\'' . Tools::sanitize($expression) . '\'';
                    }

                    if($key !== $last) {
                        $property .= ',';
                    }
                }
                $property .= ']';
            }

            $code = str_replace($node->booleanAttributes[$i]['hook'], '<?php SkinEngine\Tools\Functions::compare(' . $property . ', \'' . $node->booleanAttributes[$i]['attribute'] . '\');?>', $code);
        }
        return $code;
    }

    /**
     * Add PHP loops.
     * @param \Element $node
     * @param type $content
     * @return type
     */
    private static function replaceLoopBlock(Element $node, $content = '')
    {
        array_push(static::$contexts, '$' . $node->loop['alias']);
        $property = $node->loop['context'] . '[\'' . $node->loop['property'] . '\']';
        $content .= '<?php if(isset(' . $property . ')){$skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_LOOP_INDEX\'] = 0;
            $skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_LOOP_COUNT\'] = count(' . $property . ');
            foreach(' . $property . ' as $' . $node->loop['alias'] . ') {
            $skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_ITEM_PLAIN\'] = SkinEngine\Tools\Functions::prepareItemPlain($' . $node->loop['alias'] . ');
            ?>' . PHP_EOL;

        $content .= '<?php SkinEngine\Tools\Functions::$current[\'itemIndex\'] = $skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_LOOP_INDEX\'];?>';
        $content .= '<?php SkinEngine\Tools\Functions::$current[\'itemCount\'] = $skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_LOOP_COUNT\'];?>';
        $content .= $node->start;
        foreach($node->children as $child) {
            $content .= static::getOutputNode($child);
            // dump(static::getOutputNode($child));
        }
        $content .= $node->end;
        array_pop(static::$contexts);
        return $content .= '<?php ++$skinLocalVars[\'$' . $node->loop['alias'] . '\'][\'SKIN_LOOP_INDEX\']; }} ?>' . PHP_EOL;
    }

    /**
     * Add PHP conditions.
     * @param \Element $node
     */
    private static function replaceConditionBlock(Element $node)
    {
        if(!empty($node->condition)) {
            $property = $node->condition['context'] . '[\'' . $node->condition['property'] . '\']';

            $output = $node->condition['conditionType'] ?
                '!isset(' . $property . ') || empty(' . $property . ') || !SkinEngine\Tools\Functions::toBoolean(' . $property . ')' :
                'isset(' . $property . ') && !empty(' . $property . ') && SkinEngine\Tools\Functions::toBoolean(' . $property . ')';
            $node->start = '<?php if(' . $output . ') { ?>' . PHP_EOL . $node->start;
            $node->end .= '<?php } ?>' . PHP_EOL;
        }
    }
}
