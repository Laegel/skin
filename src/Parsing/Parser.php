<?php
namespace SkinEngine\Parsing;

use \SkinEngine\Skin,
    \SkinEngine\Exception\ExceptionHandler,
    \SkinEngine\Elements\Element,
    \SkinEngine\Parsing\Replacer,
    \SkinEngine\Tools\Tools,
    \SkinEngine\Tools\Group;

class Parser
{

    private static $attributeAlias = [
            '.' => 'class',  '#' => 'id',       '?' => 'skif',
            '%' => 'skloop', '$' => 'sksample', ':' => 'skelse',
            '*' => 'sktext',
        ], $sampleManager, $parsedTags = [];

    static $skinAttributes = [
            'skif', 'skloop', 'sksample', 'skelse', 'sktext'
        ], $content, $viewsDir, $samplesDir, $samplesVariable,
        $fileName, $outputMode,
        $errorReport, $currentLine = 0, $contexts = [], $structure = [];

    const   PARSE_ATTR_START = '~([\w-]*|[',
            PARSE_ATTR_END = '])\(([^"]+?)\) ~S',
            PARSE_ATTR_SHORT_START = '~(?<!\w)([',
            PARSE_ATTR_SHORT_END = '])([\w-;{|}=>.#/]+)~S',
            PARSE_COMMENTS = '~[\s](?://)[^\r\n]*|/\*.*?\*/~s',
            REGEX_SUB_VAR_BLOCK = '~([^"]*)\[\[([\w .$#_]*)\]\]([^"]*)~',
            REGEX_VAR_BLOCK = '~([$]?){{([\w .$#_\[\]]*)\|?([\w ,\(\);]*)}}~S',
            REGEX_TRANSLATE_BLOCK = '~([$]?){=([\w .$#_\[\]]*)\|?([\w .,$#_]*)=}~S',
            PARSE_PARTIAL = '~@partial ([^\r\n\s]*)~',
            REGEX_PARTIAL_INDENT = '~([\h]*)@partial~',
            PARSE_EXTENDS = '~@extends ([^\r\n\s]*)~',
            REGEX_EXTENDS_INDENT = '~([\h]*)@content~',
            FILE_EXTENSION = '.skln', OUTPUT_MODE_MINI = 0, OUTPUT_MODE_FULL = 1,
            ERROR_REPORT_NONE = 0, ERROR_REPORT_WARNINGS = 1, ERROR_REPORT_ERRORS = 2, ERROR_REPORT_ALL = 3,
            VAR_GLOBAL_CONTEXT = 0, VAR_LOOP_CONTEXT = 1, VAR_LOOP_PROPERTY_CONTEXT = 2;

    public static function addParsedTag($tag)
    {
        array_push(static::$parsedTags, $tag);
    }

    /**
     * Launching function.
     * @param string $fileName
     * @param string $type
     * @param integer $mode
     * @param integer $report
     * @return string
     */
    public static function parseContent($fileName, $mode = 0, $report = 0)
    {
        static::$outputMode = $mode;
        static::$errorReport = $report;
        static::$fileName = $fileName;
        $structure = static::getStructure($fileName);
        static::$structure = $structure;
        $template = static::executeSkin($structure);
        ExceptionHandler::displayParsed($structure);
        return $template;
    }

    public static function parseString($string)
    {
        $string = static::stripComments($string);
        $temp = array_values(
            array_filter(explode("\n", str_replace("\r\n", "\n", $string)), (
                function($input) {
                    if(0 !== strlen(trim($input)))
                        return $input; //Filter blank lines
                })
            )
        );
        array_unshift($temp, '');

        return static::executeSkin(static::parseLines($temp));
    }

    /* SETTERS */

    /**
     * Set output mode.
     * @param string $mode
     */
    public static function setOutputMode($mode)
    {
        static::$outputMode = $mode;
    }

    /**
     * Set views directory.
     * @param string $dir
     */
    public static function setViewsDir($dir)
    {
        static::$viewsDir = $dir;
    }

    /**
     * Set samples directory.
     * @param string $dir
     */
    public static function setSamplesDir($dir)
    {
        static::$samplesDir = $dir;
    }

    /**
     * Set samples JavaScript variable.
     * @param string $dir
     */
    public static function setSamplesVariable($variable)
    {
        static::$samplesVariable = $variable;
    }


    /* UTILS */

    private static function stripComments($content)
    {
        return preg_replace(static::PARSE_COMMENTS, '', $content);
    }

    /**
     * Get skeleton file content if it exists.
     * @param string $file
     * @return string
     */
    private static function extractContent($file)
    {
        if(file_exists(static::$viewsDir . static::$fileName . static::FILE_EXTENSION))
            return static::stripComments(file_get_contents(static::$viewsDir . static::$fileName . static::FILE_EXTENSION));
        else {
            ExceptionHandler::addError(ExceptionHandler::E_MAIN_TEMPLATE_404, ['TEMPLATE' => $file]);
            return "\r\n";
        }
    }

    /**
     * Get attribute alias if it exists.
     * @param string $attribute
     * @return string
     */
    private static function getAttributeAlias($attribute)
    {
        return isset(static::$attributeAlias[$attribute]) ? static::$attributeAlias[$attribute] : $attribute;
    }

    /**
     * Return alias list "static::$attributeAlias" as a string with all indexes (used in regex).
     * @return string
     */
    private static function aliasesAsString()
    {
        $aliases = array_keys(static::$attributeAlias);
        $countAliases = count($aliases);
        $string = '';
        for($i = 0; $i < $countAliases; ++$i)
            $string .= $aliases[$i];
        return $string;
    }

    /**
     * Get Skin attributes (skif, skelse, skloop, sksample)
     * @param Element $node
     * @return array
     */
    private static function manageSkinAttributes(Element $node)
    {
        $skinAttributes = [];
        if(!empty($node->attributes)) {
            $countAttr = count($node->attributes[0]);
            for($attr = 0; $attr < $countAttr; ++$attr)
                if(in_array($node->attributes[0][$attr], static::$skinAttributes))
                    $skinAttributes[$node->attributes[0][$attr]] = $node->attributes[1][$attr];
        }
        return $skinAttributes;
    }

    /**
     * Sort the template lines by indent and new lines hierarchy as a multidimensional array by node => children.
     * @param array $dataset
     * @return array
     */
    private static function mapTree(array $dataset)
    {
        $tree = [];
        foreach($dataset as $id => &$node) {
            if(null === $node->parent)
                $tree[$id] = &$node;
            else {
                if(!isset($dataset[$node->parent]->children))
                    $dataset[$node->parent]->children = [];
                $dataset[$node->parent]->children[$id] = &$node;
            }
        }
        return $tree;
    }

    /**
     * Create spaces depending on the node level.
     * @param integer $level
     * @param string $space
     * @return string
     */
    public static function setSpacesByLevel($level, $space = ' ')
    {
        $tabs = '';
        while($level > 0) {
            $tabs .= $space;
            --$level;
        }
        return $tabs;
    }

    /**
     * Get the template structure, line by line.
     * @param string $fileName
     * @return array
     */
    private static function getStructure($fileName)
    {
        $temp = array_values(array_filter(explode("\n", str_replace("\r\n", "\n", static::parseSpecial(static::extractContent($fileName)))), (function($input) {
                    if(0 !== strlen(trim($input)))
                        return $input; //Filter blank lines
                })));
        array_unshift($temp, '');

        return static::parseLines($temp);
    }

    /**
     * Get skeleton file content (the partial template) if it exists.
     * @param string $content
     * @return string
     */
    private static function fetchPartial($content)
    {
        preg_match(static::REGEX_PARTIAL_INDENT, $content, $tabs);
        $tabs = empty($tabs) ? '' : $tabs[1];
        preg_match_all(static::PARSE_PARTIAL, $content, $matchPartial);
        $countPartial = count($matchPartial[0]);
        for($partials = 0; $partials < $countPartial; ++$partials) {
            $part = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, static::$viewsDir . $matchPartial[1][$partials] . static::FILE_EXTENSION);
            if(file_exists($part)) {
                $partialView = static::stripComments(file_get_contents($part));
                $partialViewExploded = explode("\n", $partialView);
                $countPartialExplode = count($partialViewExploded);
                for($i = 1; $i < $countPartialExplode; ++$i)
                    $partialViewExploded[$i] = $tabs . $partialViewExploded[$i];
                $partialView = implode("\n", $partialViewExploded);
            } else {
                ExceptionHandler::addError(ExceptionHandler::E_PARTIAL_TEMPLATE_404, ['TEMPLATE' => $matchPartial[1][$partials]]);
                $partialView = '';
            }
            $content = static::fetchPartial(str_replace($matchPartial[0][$partials], $partialView, $content));
        }
        return $content;
    }

    /**
     * Get skeleton file content (the parent template) if it exists.
     * @param string $content
     * @return string
     */
    private static function fetchParent($content)
    {
        preg_match(static::PARSE_EXTENDS, $content, $matchParent);
        if(isset($matchParent[1])) {
            $matchParent[1] = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $matchParent[1]);
            if(file_exists(static::$viewsDir . $matchParent[1] . static::FILE_EXTENSION)) {
                $parentView = static::stripComments(file_get_contents(static::$viewsDir . $matchParent[1] . static::FILE_EXTENSION));
                preg_match(static::REGEX_EXTENDS_INDENT, $parentView, $tabs);

                if(!isset($tabs)) {
                    ExceptionHandler::addError(ExceptionHandler::E_CONTENT_TOKEN_MISSING, ['TEMPLATE' => $matchParent[1]]);
                    $tabs = '';
                } else
                    $tabs = $tabs[1];
            } else {
                ExceptionHandler::addError(ExceptionHandler::E_PARENT_TEMPLATE_404, ['TEMPLATE' => $matchParent[1]]);
                $parentView = '';
                $tabs = '';
            }
        }
        return !empty($matchParent) ? str_replace('@content', str_replace([$matchParent[0], "\n"], ['', "\n" . $tabs], $content), $parentView) : $content;
    }

    /* PARSING */

    /**
     * Get the tag.
     * @param string $input
     * @return string
     */
    public static function parseTag($input)
    {
        $tag = strtolower(explode(' ', $input)[0]);
        return $tag;
    }

    private static function mergeAttributeList($deduplicate, $attributeList)
    {
        return [
            array_values($deduplicate),
            array_values(array_intersect_key($attributeList, $deduplicate))
        ];
    }

    /**
     * Get all the attributes from a node.
     * @param string $input
     * @return array
     */
    public static function parseAttributes(Element $node)
    {
        $input = preg_replace('~' . $node->tag . '~', '', $node->input, 1) . ' ';//Simple trick to force regex to match inner parenthesis

        $attributeAliases = static::aliasesAsString();

        preg_match_all(static::PARSE_ATTR_START . $attributeAliases . static::PARSE_ATTR_END, $input, $attributes);
        foreach($attributes[0] as $attribute) {
            $input = str_replace($attribute, '', $input);
        }
        preg_match_all(static::PARSE_ATTR_SHORT_START . $attributeAliases . static::PARSE_ATTR_SHORT_END, $input, $shortHandAttributes);
        foreach($shortHandAttributes[0] as $shortHandAttribute) {
            $input = str_replace($shortHandAttribute, '', $input);
        }

        if(!empty(trim($input))) {
            $node->type = Element::ELEMENT_TYPE_TEXT;
            return [];
        }

        $countAttributes = count($attributes[1]);
        for($i = 0; $i < $countAttributes; ++$i)
            $attributes[1][$i] = static::getAttributeAlias($attributes[1][$i]);

        $countShortHandAttributes = count($shortHandAttributes[1]);
        for($i = 0; $i < $countShortHandAttributes; ++$i)
            $shortHandAttributes[1][$i] = static::getAttributeAlias($shortHandAttributes[1][$i]);
        if(!empty($attributes) && !empty($shortHandAttributes)) {
            $attributeList = static::mergeAttributeList(
                array_unique(array_merge($shortHandAttributes[1], $attributes[1])),
                array_merge($shortHandAttributes[2], $attributes[2])
            );
        } elseif(empty($attributes) && !empty($shortHandAttributes)) {
            $attributeList = static::mergeAttributeList(
                array_unique($shortHandAttributes[1]),
                $shortHandAttributes[2]
            );
        } elseif(!empty($attributes) && empty($shortHandAttributes)) {
            $attributeList = static::mergeAttributeList(
                array_unique($attributes[1]),
                $attributes[2]
            );
        } else
            $attributeList = [];
        return $attributeList;
    }

    /**
     * Make objects from the templates lines.
     * @param array $inputs
     * @return array of Element
     */
    private static function parseLines(array $inputs)
    {
        $skinLayer = Tools::getLayer();
        $countInputs = count($inputs);
        $structure = [];
        for($i = 1; $i < $countInputs; ++$i) {
            static::$currentLine = $i;
            $structure[$i] = new Element($i, $inputs[$i]);
            $structureCount = count($structure);
            for($j = $structureCount; $j > 0; --$j)
                if($structure[$j]->level < $structure[$i]->level) {
                    $structure[$i]->parent = $j;
                    $structure[$i]->toParse = $structure[$j]->toParse && !in_array($structure[$j]->tag, $skinLayer::$specialContainers);
                    if(!$structure[$i]->toParse) {
                        $structure[$i]->type = Element::ELEMENT_TYPE_TEXT;
                        $structure[$i]->tag = '';
                    }
                    if($structure[$i]->isText()) {
                        $structure[$i]->toParse = false;
                    }
                    if(!empty($structure[$j]->attributes)) {//If parent has sktext attribute, children are treated as plaintext
                        foreach($structure[$j]->attributes[0] as $attribute) {
                            if('sktext' === $attribute) {
                                $structure[$i]->type = Element::ELEMENT_TYPE_TEXT;
                                $structure[$i]->tag = '';
                                $structure[$i]->toParse = false;
                                break;
                            }
                        }
                    }
                    break;
                }
        }
        return $structure;
    }

    /**
     * Execute special parsing : fetch parent and partial templates.
     * @param string $content
     * @return string
     */
    private static function parseSpecial($content)
    {
        return static::fetchPartial(static::fetchParent($content));
    }

    /**
     * Parse properties in conditions, loops, variables, and translations.
     * @param string $propertyString
     * @return array
     */
    public static function parseProperty($propertyString)
    {
        if(!empty($propertyString)) {
            $variable = str_replace(' ', '', $propertyString);
            $parsedProperty = explode('.', $variable);

            if(!isset($parsedProperty[1])) {
                $type = static::VAR_GLOBAL_CONTEXT;
                $context = 'static::$data';
                $property = $parsedProperty[0];
            } else {
                if(in_array($parsedProperty[0], static::$contexts)) {
                    $type = static::VAR_LOOP_CONTEXT;
                    $context = '$' . $parsedProperty[0];
                    $property = $parsedProperty[1] . (isset($parsedProperty[2]) ? '.' . $parsedProperty[2] : '');
                } else {
                    $type = static::VAR_LOOP_PROPERTY_CONTEXT;
                    $context = 'static::$data[\'' . $parsedProperty[0] . '\']';
                    $property = $parsedProperty[1];
                }
            }
            return ['type' => $type, 'context' => $context, 'property' => $property, 'inputParsed' => $parsedProperty];
        }
        return [];
    }

    /**
     * Parse functions in variable blocks.
     * @param string $functionString
     * @return array
     */
    private static function parseFunctions($functionString)
    {
        $functions = explode(',', str_replace(' ', '', $functionString));
        $countFunctions = count($functions);
        $returnedFunctions = [];
        for($i = 0; $i < $countFunctions; ++$i) {
            $argsPosition = strpos($functions[$i], '(');
            $arguments = [];
            if(false !== $argsPosition) {
                $input = $functions[$i];
                $functions[$i] = substr($input, 0, $argsPosition);
                $arguments = array_filter(explode(';', str_replace(['(', ')'], '', substr($input, $argsPosition, strlen($input)))),
                    function($value) {
                        if(!empty($value))
                            return $value;
                    });
            }

            if(Group::exists($functions[$i]))
                array_push($returnedFunctions, ['function' => $functions[$i], 'arguments' => $arguments]);
            elseif(!empty($functions[$i]))
                ExceptionHandler::addWarning(ExceptionHandler::W_FUNCTION_UNDEFINED, ['FUNCTION' => $functions[$i], 'LINE' => static::$currentLine]);
        }
        return $returnedFunctions;
    }

    /* PREPARING DATA FOR REPLACEMENT */

    /**
     * Convert the default structure to node => children hierarchy.
     * @param array $structure
     * @return array
     */
    private static function executeSkin(array $structure)
    {
        $tree = static::mapTree($structure);

        foreach($tree as $node) {
            static::buildTemplate($node);
        }

        static::setSampleFile();
        $skinLayer = Tools::getLayer();
        $elements = Tools::arrayToObject($skinLayer::getElements());

        foreach($elements as $tag => $element) {
            if(isset($element->required) && !in_array($tag, static::$parsedTags))
                ExceptionHandler::addWarning(
                    ExceptionHandler::W_TAG_MISSING, ['TAG' => $tag]
                );
        }
        return $tree;
    }

    /**
     * Prepare the nodes.
     * @param Element $node
     */
    public static function buildTemplate(Element $node)
    {
        static::$currentLine = $node->line;

        if(!$node->toParse || $node->isText()) {//Set children level relative to the parent
            $node->level = 0;
            if(!empty($node->parent) && isset(static::$structure[$node->parent])) {
                //Escape PHP tags
                $node->input = str_replace(['<?', '?>'], ['&lt;?', '?&gt;'], $node->input);

                $node->clear();
                $parent = static::$structure[$node->parent];

                if($parent->isText()) {
                    $node->level = $parent->level + 4;
                }
            }

            $node->start = $node->input;
            $node->end = '';
        }
        $node->replaceAttributes();
        $node->setOutputLevel();

        static::prepareOutputNode($node);

        foreach($node->children as $child) {
            static::buildTemplate($child);
        }
    }

    /**
     * Prepare node.
     * @param Element $node
     * @return string
     */
    public static function prepareOutputNode(Element $node)
    {
        $skinAttributes = static::manageSkinAttributes($node); //Get Skin attributes (skif, skloop and sksample)
        $content = '';
        if(isset($skinAttributes['sksample']))
            static::prepareSample($node, $skinAttributes['sksample']);

        if(isset($node->ignore) && $node->ignore) {
            $node->input = '';
            $node->start = '';
            $node->end = '';
            return;
        }
        if(isset($skinAttributes['skif']))
            $node->setCondition(static::parseProperty($skinAttributes['skif']), false);
        elseif(isset($skinAttributes['skelse']))
            $node->setCondition(static::parseProperty($skinAttributes['skelse']), true);

        if(isset($node->children) && !empty($node->children)) {
            if(isset($skinAttributes['skloop'])) {//If element has skloop, data context is set inside the corresponding array
                $explodedLoopAttribute = explode('=>', $skinAttributes['skloop']);
                $parsedProperty = static::parseProperty($explodedLoopAttribute[0]);
                $node->setLoop($parsedProperty, $explodedLoopAttribute[1]);

                $context = 0 === $parsedProperty['type'] ? 'GLOBAL' : $parsedProperty['context'];
                Replacer::addDataContext($context, [
                    'index' => '$' . $explodedLoopAttribute[1],
                    'data' => $parsedProperty['context'] . '[\'' . $parsedProperty['property'] . '\']'
                ]);
                if(!in_array($explodedLoopAttribute[1], static::$contexts))
                    array_push(static::$contexts, $explodedLoopAttribute[1]);
            }
        }

        static::prepareVariableBlocks($node, $node->start . $content . $node->end);
        static::prepareTranslateBlocks($node, $node->start . $content . $node->end);
    }

    public static function prepareSubVariableBlocks($code)
    {
        preg_match_all(static::REGEX_SUB_VAR_BLOCK, $code, $matchVariable);
        if(isset($matchVariable[2][0])) {
            $parsed = static::parseProperty($matchVariable[2][0]);
            return (!empty($matchVariable[1][0]) ? '\'' . $matchVariable[1][0] . '\' . ' : '') .
                (isset(Replacer::$specialProperties[$parsed['property']]) ?
                '$skinLocalVars[\'' . $parsed['context'] . '\'][' . Replacer::$specialProperties[$parsed['property']] . ']' :
                $parsed['context'] . '[\'' . $parsed['property'] . '\']') .
                (!empty($matchVariable[3][0]) ? ' . \'' . $matchVariable[3][0] . '\'' : '');
        }
        return '\'' . $code . '\'';
    }

    /**
     * Replace variable blocks from the current line.
     * @param string $code
     * @return string
     */
    public static function prepareVariableBlocks($node, $code)
    {

        preg_match_all(static::REGEX_VAR_BLOCK, $code, $matchVariable);

        $countVariable = count($matchVariable[1]);
        if($countVariable > 0) {
            for($variables = 0; $variables < $countVariable; ++$variables) {
                if('$' !== $matchVariable[1][$variables]) {
                    $parsedVariable = static::parseProperty($matchVariable[2][$variables]);
                    $parsedFunctions = static::parseFunctions($matchVariable[3][$variables]);
                    $node->addVariable($matchVariable[0][$variables], $parsedVariable, $parsedFunctions);
                } else {
                    $node->start = str_replace($matchVariable[0][$variables], substr($matchVariable[0][$variables], 1), $node->start);
                }
            }
        }
        return $code;
    }

    /**
     * Replace translation block from the current line.
     * @param string $code
     * @return string
     */
    private static function prepareTranslateBlocks(Element $node, $code)
    {
        preg_match_all(static::REGEX_TRANSLATE_BLOCK, $code, $matchTranslate);
        $countTranslate = count($matchTranslate[1]);
        if($countTranslate > 0) {
            for($translates = 0; $translates < $countTranslate; ++$translates) {
                if('$' !== $matchTranslate[1][$translates]) {
                    $explodeTranslate = explode(',', str_replace(' ', '', $matchTranslate[3][$translates]));

                    $countVariables = count($explodeTranslate);
                    $variables = [];
                    for($i = 0; $i < $countVariables; ++$i) {
                        $parsedVariable = static::parseProperty($explodeTranslate[$i]);
                        if(!empty($parsedVariable))
                            array_push($variables, $parsedVariable);
                    }
                    $node->addTranslation(['input' => $matchTranslate[0][$translates], 'index' => str_replace(' ', '', $matchTranslate[2][$translates]), 'variables' => $variables]);
                } else {
                    $node->start = str_replace($matchTranslate[0][$translates], substr($matchTranslate[0][$translates], 1), $node->start);
                }

            }
        }
        return $code;
    }

    private static function getChildrenInputs(&$sampleContent, Element $node, $toParse)
    {
        $countAttributes = count($node->attributes[0]);
        $levelZero = !isset($sampleContent['levelZero']) ?
            $sampleContent['levelZero'] = $node->level : $sampleContent['levelZero'];
        $input = static::setSpacesByLevel($node->level - $levelZero) . $node->input;
        for($i = 0; $i < $countAttributes; ++$i) {
            if(isset($node->attributes[0][$i]) && 'sksample' === $node->attributes[0][$i]) {//Remove samples in samples
                $input = str_replace(['$' . $node->attributes[1][$i], '$(' . $node->attributes[1][$i] . ')'], '', $input);
                unset($node->attributes[0][$i]);
                unset($node->attributes[1][$i]);
                $node->attributes[0] = array_values($node->attributes[0]);
                $node->attributes[1] = array_values($node->attributes[1]);
            }
        }
        $sampleContent['tree'] .= $input . "\n";

        if(isset($node->children)) {
            foreach($node->children as &$child) {
                static::getChildrenInputs($sampleContent, $child, $toParse);
            }
        }
        if(!$toParse) {
            $node->ignore = true;
        }
    }

    /*SAMPLING IS GONNA BE REWORKED*/
    private static function prepareSample(Element $node, $sampleName)
    {
        $explodedSample = explode(';', $sampleName);
        $fileName = static::$samplesDir . static::$fileName . 'Samples.js';
        $sampleContent = ['tree' => ''];
        $toParse = true;
        if(isset($explodedSample[1]) && 'false' === strtolower($explodedSample[1])) {
            $node->ignore = true;
            $toParse = false;
        }
        static::getChildrenInputs($sampleContent, $node, $toParse);
        static::$sampleManager[$explodedSample[0]] = $sampleContent['tree'];
    }

    private static function setSampleFile()
    {
        return 0;
        if(!is_dir(static::$samplesDir)) {
            ExceptionHandler::addError(ExceptionHandler::E_SAMPLE_FOLDER_MISSING, ['DIR' => static::$samplesDir]);
            return NULL;
        }
        $exploded = explode(DIRECTORY_SEPARATOR, str_replace(['/', '\\'], DIRECTORY_SEPARATOR, static::$fileName));
        if(isset($exploded[1])) {
            if(!is_dir(static::$samplesDir . $exploded[0])) {
                ExceptionHandler::addError(ExceptionHandler::E_SAMPLE_FOLDER_MISSING, ['DIR' => static::$samplesDir . $exploded[0]]);
                return NULL;
            }
        }
        $fileName = static::$samplesDir . static::$fileName . 'Samples.js';
        file_put_contents($fileName, static::$samplesVariable . ' = ' . json_encode(static::$sampleManager));
    }
}
