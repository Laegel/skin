<?php
return [
	'lists' => [
		'LANGUAGES' => [
			['value' => 'en', 'name' => 'Anglais'],
		    ['value' => 'fr','name' => 'Français']
		],
	],
	'translations' => [
		'TITLE' => 'Didacticiel de Skin',
		'LANGUAGE' => 'Langue',
		'SKIN_TUTORIAL' => 'Didacticiel de Skin',
		'SUB_TITLE' => '',
		'DESCRIPTION' => '',
		'WRITE_NAME' => 'Inscrivez ici votre prénom : ',
		'WELCOME_NAME' => [
			'Bienvenue %s !', 'Salut %s, prêt à commencer ?', 'C\'est parti pour travailler avec Skin, %s !'
		],
		'SUBMIT' => [
			'C\'est parti !', 'Lancer !'
		],
		'TEST_HIDDEN' => 'Je suis un échantillon caché !',
		'TEST' => 'TEST %s',
		'FOR_EXAMPLE' => 'Exemple : ',
		'START' => 'Débuter',
		'LOOPS' => 'Boucles',
		'ELEMENTS' => 'Elements',
		'VARIABLES' => 'Variables',
		'STAR_N' => [
			'one' => '%s étoile', 'other' => '%s étoiles'
		],
		'ELEMENTS_INTRO' => 'Les éléments ',
		'VARIABLES_INTRO' => '',
		'RESULT' => 'Résultat',
		'TABLE_EXAMPLE' => 'Exemple avec "table"',
		'TABLE_EXPLANATION' => 'In this example, we create a simple table with headers and a two-dimensional array of data.',
		'DEFINE_LOOP' => 'Define a loop',
		'LOOP_EXPLANATION' => '
		p
			To define a loop, you have to use a Skin special attribute called "skloop", which alias is the percentage (%) character.
		p
			Inside "skloop" attributes, you type the variable you want to loop on and its alias, separated by an arrow (=>), pretty much like in a "foreach" statement.
			This is the only way to start a loop.
		p
			A loop scopes variables, so you can only use aliases inside of the related loops.
		p
			Loops have special properties that you can only access inside their scope.
		p
			"$" represents the loop\'s length.
		p
			"#" represents the loop\'s current index.
		p
			"_" represents the loop\'s value, if not an array.
		',
		'SIMPLE_EXAMPLE' => 'Simple example',
		'SIMPLE_EXPLANATION' => 'Here, we create a simple list with items',
		'DOWNLOAD' => 'Je le veux !',
	]
];
