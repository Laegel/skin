<?php
return [
	'lists' => [
		'LANGUAGES' => [
			['value' => 'en', 'name' => 'English'],
		    ['value' => 'fr','name' => 'French']
		],
	],
	'translations' => [
		'TITLE' => 'Skin tutorial',
		'LANGUAGE' => 'Language',
		'SKIN_TUTORIAL' => 'Skin tutorial',
		'SUB_TITLE' => 'A template engine that combines abstract tag elements plus most of popular template engine functionalities.',
		'DESCRIPTION' => 'Template inheritance, auto escaping variables, components ...',
		'WRITE_NAME' => 'Write your name here : ',
		'WELCOME_NAME' => [
			'Welcome %s !', 'Hi %s, ready to go ?', 'Let\'s work with Skin, %s !'
		],
		'SUBMIT' => [
			'Let\'s go !', 'Launch !'
		],
		'TEST_HIDDEN' => 'I am a hidden sample !',
		'TEST' => 'TEST %s',
		'FOR_EXAMPLE' => 'Example : ',
		'START' => 'Getting started',
		'LOOPS' => 'Loops',
		'ELEMENTS' => 'Tags',
		'VARIABLES' => 'Variables',
		'STAR_N' => [
			'zero' => '%s stars', 'one' => '%s star', 'other' => '%s stars'
		],
		'ELEMENTS_INTRO' => 'Tags are the first thing to write in an element. Unlike HTML, forget chevrons : just write its name.',
		'VARIABLES_INTRO' => 'Variables are written like in many other template engines, between two pairs of braces like this : ${{example}}.',
		'RESULT' => 'Result',
		'TABLE_EXAMPLE' => '"Table" example',
		'TABLE_EXPLANATION' => 'In this example, we create a simple table with headers and a two-dimensional array of data.',
		'DEFINE_LOOP' => 'Define a loop',
		'LOOP_EXPLANATION' => '
		p
			To define a loop, you have to use a Skin special attribute called "skloop", which alias is the percentage (%) character.
		p
			Inside "skloop" attributes, you type the variable you want to loop on and its alias, separated by an arrow (=>), pretty much like in a "foreach" statement.
			This is the only way to start a loop.
		p
			A loop scopes variables, so you can only use aliases inside of the related loops.
		p
			Loops have special properties that you can only access inside their scope.
		p
			"$" represents the loop\'s length.
		p
			"#" represents the loop\'s current index.
		p
			"_" represents the loop\'s value, if not an array.
		',
		'SIMPLE_EXAMPLE' => 'Simple example',
		'SIMPLE_EXPLANATION' => 'Here, we create a simple list with items',
		'DOWNLOAD' => 'I want it !',
	]
];
