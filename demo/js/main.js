$(function() {
    $(document).ready(function() {
        $('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
        });

        $('#language').dropdown();

        var hi = $('#hi');

        hi.one('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var data1 = [
                'vale-ntin', 'cho-uaf'
            ], data2 = [
                'gm-ail', 'c-om'
            ], string = '';

            for(var i = 0; i < data1.length; ++i) {
                data1[i] = data1[i].replace('-', '');
                data2[i] = data2[i].replace('-', '');
            }

            var txt = document.createElement("textarea");
            txt.innerHTML = '&commat;';


            string = data1.join('.') + txt.value + data2.join('.');
            hi.html(hi.html() + ' : ' + string);
        });
    });
});
