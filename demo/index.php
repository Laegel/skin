<?php
require '../vendor/autoload.php';
require 'lib/Spyc.php';


if(!defined('CURRENT_DIR'))
	define('CURRENT_DIR', '../');
define('DEMO_DIR', CURRENT_DIR . 'demo' . DIRECTORY_SEPARATOR);

function output($data)
{
	echo '<pre>';
	print_r(htmlentities($data));
	echo '</pre>';
}

function dump()
{
	echo '<pre>';
	call_user_func_array('var_dump', func_get_args());
	echo '</pre>';
}

function __()
{
	return call_user_func_array('SkinEngine\Translator\Translator::translate', func_get_args());
}

function test($string)
{
	return 'test' . $string;
}

$tutoLang = [
	'en', 'fr'
];

$languages = ['en_GB', 'fr_FR'];

SkinEngine\Translator\Language::registerLanguage('en_GB');
SkinEngine\Translator\Language::registerLanguage('fr_FR');

if(isset($_GET['language']) && in_array($_GET['language'], $tutoLang)) {
	$language = array_search($_GET['language'], $tutoLang);
} else {
	$language = 0;
}

SkinEngine\Translator\Translator::setLanguage($languages[$language]);
SkinEngine\Translator\Translator::setTranslationsDir(DEMO_DIR . 'translations' . DIRECTORY_SEPARATOR);

SkinEngine\Translator\Translator::setDataCallback(function($path) {
	return Spyc::YAMLLoad($path . '.yaml');
});

/*Setting data that will be used in the templates*/
$data = new stdClass();//$data can be an object or an array !

$data->isNameSet = isset($_POST['name']);
$data->name = $data->isNameSet ? $_POST['name'] : '';
$data->amount = 1;

$data->languages = SkinEngine\Translator\Translator::_l('LANGUAGES');
$data->Blabla = 12;
$data->title = 'title';
$data->navItems = [
	['title' => __('INSTALL'), 'link' => 'install'],
	['title' => __('CONFIG'), 'link' => 'config'],
	['title' => __('TPL'), 'link' => 'templates'],
	['title' => __('ELEMENTS'), 'link' => 'elements'],
	['title' => __('VARIABLES'), 'link' => 'variables'],
	['title' => __('TRANSLATIONS'), 'link' => 'translations'],
	['title' => __('LOOPS'), 'link' => 'loops'],
	['title' => __('COMPONENTS'), 'link' => 'components'],
	//['title' => __('SAMPLES'), 'link' => 'samples'],
];


$data->bla = 'YAY';

$data->bleups = [
	'BliBlo', 'BliBle', 'BliBla'
];

$data->blops = [
	['name' => 'BliBlo'],
	['name' => 'BliBle'],
	['name' => 'BliBla']
];

$data->machins = [
	['BliBlo' => 'blo', 'BliBle' => 'ble', 'BliBla' => 'bla'],
	['BliBlo' => 'blô', 'BliBle' => 'blê', 'BliBla' => 'blâ'],
	['BliBlo' => 'blö', 'BliBle' => 'blë', 'BliBla' => 'blä']
];

//Tuto vars

$data->world = 'World';
$data->name = 'John';
$data->age = 32;

$data->dynamic = 'Yeah !';
$data->varVar = 'dynamic';

$data->test = 'test';
$data->foo = 'bar';
$data->index = [
	'foo' => 'bar'
];

$data->headers = [
	'Name', 'Value'
];

$data->rows = [
	[
		'name' => 'Bla',
		'value' => 'Hap'
	],
	[
		'name' => 'Bli',
		'value' => 'Hip'
	],
	[
		'name' => 'Blo',
		'value' => 'Hop'
	]
];

$data->table = [
	'headers' => [
		'Bla', 'Bli', 'Blo'
	],
	'rows' => [
		[
			'cells' => [
				'Hap', 'Hip', 'Hép'
			]
		]
	]
];

$data->items = [
	[
		'name' => 'Foo'
	],
	[
		'name' => 'Bar'
	]
];

SkinEngine\Parsing\Replacer::setWalker(function($node) {
	if('script' === $node->tag) {

		$node->addAttribute('test', 'blop');
		$node->removeAttribute('test');
	}
});


function stringToFoobar($string)
{
	return 'foobar';
}

function urlLang($string)
{
	if(false !== strpos($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'language=')) {
		$url = preg_replace('~[&|?]language=([\S]*)~', '?language=' . $string, $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	} else
		$url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '?language=' . $string;
	return 'http://' . $url;
}

function linkIn($link)
{
	if(false !== strpos($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 'nav=')) {
		$url = preg_replace('~[&|?]nav=([\S]*)~', '?nav=' . $link, $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	} else
		$url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '?nav=' . $link;
	return 'http://' . $url;
}

function inc($value)
{
	return ++$value;
}

SkinEngine\Tools\Group::register('linkIn', [
	'linkIn'
]);

SkinEngine\Tools\Group::register('inc', [
	'inc'
]);

SkinEngine\Tools\Group::register('foobarify', [
	'stringToFoobar'
]);

SkinEngine\Tools\Group::register('urlLang', [
	'urlLang'
]);

SkinEngine\Elements\Component::$path = DEMO_DIR . 'components' . DIRECTORY_SEPARATOR;
SkinEngine\Elements\Component::register('block', [
	'attributes' => ['content']
]);

SkinEngine\Elements\Component::register('table-data', [
	'attributes' => ['data']
]);

SkinEngine\Elements\Component::register('checkbox', [
	'attributes' => ['id', 'name', 'checked']
]);

if(!isset($_GET['nav']))
	header('Location: ?nav=install');

$match = false;
foreach($data->navItems as $nav) {
	if($nav['link'] === $_GET['nav']) {
		$match = true;
		break;
	}
}

if(!$match)
	header('Location: ?nav=install');

$anchors = [
	'loops' => [
		['id' => 'ex1', 'name' => __('EXAMPLE', [1])],
		['id' => 'ex2', 'name' => __('EXAMPLE', [2])],
		['id' => 'ex3', 'name' => __('EXAMPLE', [3])],
	]
];

$nav = $_GET['nav'];

if(isset($anchors[$nav]))
	$data->anchors = $anchors[$nav];

/*Initializing Skin*/
$view = new SkinEngine\Skin(
    $data, [
		'viewsDir' => DEMO_DIR . 'views' . DIRECTORY_SEPARATOR,
		'samplesDir' => DEMO_DIR . 'samples' . DIRECTORY_SEPARATOR,
	    'errorReport' => SkinEngine\Parsing\Parser::ERROR_REPORT_NONE,
	    'outputType' => SkinEngine\Parsing\Parser::OUTPUT_MODE_FULL,
	], 'HTML6'
);

$view->setSamplesVariable('Carpenter.Samples')->setCacheDir(DEMO_DIR . 'cache' . DIRECTORY_SEPARATOR);

$view->output('steps/' . $nav, true);//Rendering !
